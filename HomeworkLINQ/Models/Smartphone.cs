﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkLINQ.Models
{
    public class Smartphone:Entity
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int MemorySize { get; set; }
        public double ScreenSize { get; set; }
        public int BatteryCapacity { get; set; }
        public string Color { get; set; }
        public int Price { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
