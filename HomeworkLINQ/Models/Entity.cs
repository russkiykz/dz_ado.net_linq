﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkLINQ.Models
{
    public abstract class Entity
    {
        public Guid Id { get; private set; } = Guid.NewGuid();
    }
}
