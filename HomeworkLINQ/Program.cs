﻿using HomeworkLINQ.DataAccess;
using HomeworkLINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeworkLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //var smartphones = new List<Smartphone>()
            //{
            //    new Smartphone()
            //    {
            //        Manufacturer="Samsung",
            //        Model = "Galaxy A02",
            //        MemorySize=32,
            //        ScreenSize=6.5,
            //        BatteryCapacity=5000,
            //        Color="Blue",
            //        Price=48700,
            //        DateAdded=new DateTime(2021,2,6)
            //    },
            //    new Smartphone()
            //    {
            //        Manufacturer="Xiaomi",
            //        Model = "Mi 10T",
            //        MemorySize=128,
            //        ScreenSize=6.67,
            //        BatteryCapacity=5000,
            //        Color="Cosmic Black",
            //        Price=229990,
            //        DateAdded = new DateTime(2020,4,12)
            //    },
            //    new Smartphone()
            //    {
            //        Manufacturer="Apple",
            //        Model = "iPhone 12",
            //        MemorySize=128,
            //        ScreenSize=6.1,
            //        BatteryCapacity=4500,
            //        Color="Green",
            //        Price=499900,
            //        DateAdded = new DateTime(2020,9,22)
            //    },
            //    new Smartphone()
            //    {
            //        Manufacturer="Apple",
            //        Model = "iPhone 12 Pro",
            //        MemorySize=256,
            //        ScreenSize=6.1,
            //        BatteryCapacity=5500,
            //        Color="Pacific Blue",
            //        Price=669900,
            //        DateAdded = new DateTime(2020,10,2)
            //    },
            //    new Smartphone()
            //    {
            //        Manufacturer="Apple",
            //        Model = "iPhone 11",
            //        MemorySize=128,
            //        ScreenSize=6.1,
            //        BatteryCapacity=4500,
            //        Color="Purple",
            //        Price=384900,
            //        DateAdded = new DateTime(2020,10,2)
            //    },
            //    new Smartphone()
            //    {
            //        Manufacturer="Meizu",
            //        Model = "M10",
            //        MemorySize=32,
            //        ScreenSize=6.5,
            //        BatteryCapacity=4000,
            //        Color="Sea Blue",
            //        Price=53900,
            //        DateAdded = new DateTime(2021,1,2)
            //    }
            //};
            //using(var context = new ShopContext())
            //{
            //    context.AddRange(smartphones);
            //    context.SaveChanges();
            //}

            using(var context = new ShopContext())
            {
                int parePage = 2;
                int currentPage = 1;
                int countProduct = context.Smartphones.Count();
                var smartphones = context.Smartphones.ToList();
                
                while (true)
                {
                    var smartphonesList = smartphones
                    .Skip((currentPage - 1) * parePage)
                    .Take(parePage)
                    .ToList();
                    Console.Clear();
                    Console.WriteLine("Список товаров: ");
                    foreach(var smartphone in smartphonesList)
                    {
                        Console.WriteLine($"Смартфон {smartphone.Manufacturer} {smartphone.Model}, {smartphone.MemorySize} Gb, {smartphone.Color} - цена {smartphone.Price} тг.");
                    }
                    Console.WriteLine("\n<= пред. страница\t след.страница =>");
                    Console.WriteLine("Сортировка: 1. Сначала дешевле; 2. Сначала дороже; 3. По новизне; 4. По названию; \nВыход - ESC");
                    switch (Console.ReadKey().Key)
                    {
                        case ConsoleKey.RightArrow:
                            if (countProduct/parePage>currentPage)
                            {
                                currentPage++;
                            }
                            break;
                        case ConsoleKey.LeftArrow:
                            if (currentPage>1)
                            {
                                currentPage--;
                            }
                            break;
                        case ConsoleKey.D1:
                            smartphones = smartphones.OrderBy(smartphone => smartphone.Price).ToList();
                            currentPage = 1;
                            break;
                        case ConsoleKey.D2:
                            smartphones = smartphones.OrderByDescending(smartphone => smartphone.Price).ToList();
                            currentPage = 1;
                            break;
                        case ConsoleKey.D3:
                            smartphones = smartphones.OrderByDescending(smartphone => smartphone.DateAdded).ToList();
                            currentPage = 1;
                            break;
                        case ConsoleKey.D4:
                            smartphones = smartphones.OrderBy(smartphone => smartphone.Manufacturer).ToList();
                            currentPage = 1;
                            break;
                        case ConsoleKey.Escape:
                            return;
                    }
                }
            }
        }
    }
}
