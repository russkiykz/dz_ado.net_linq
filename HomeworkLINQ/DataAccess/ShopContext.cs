﻿using HomeworkLINQ.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkLINQ.DataAccess
{
    public class ShopContext:DbContext
    {
        public ShopContext():base()
        {
            Database.EnsureCreated();
        }

        public DbSet<Smartphone> Smartphones { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-0V33MK0\\MSSQLSERVER01; Database = HomeworkLINQ; Trusted_Connection=true;");
        }
    }
}
